using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallPlatform : MonoBehaviour
{
    public float timer, timer2, timer3;
    public GameObject Wall;


    private Rigidbody2D rb;
    private float timerAlt;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            timer -= Time.deltaTime;

            if (timer < 0)
            {
                rb.isKinematic = false;
            }
        }
        if (collision.gameObject.CompareTag("Fall"))
        {

            timer2 -= Time.deltaTime;

            if (timer2 < 0)
            {
                rb.isKinematic = false;
            }
        }
        
    }
    
}
