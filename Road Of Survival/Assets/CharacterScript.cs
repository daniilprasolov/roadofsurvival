using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class CharacterScript : MonoBehaviour
{
    public float Speed, JumpForce;
    public LayerMask groundLayer;
    public string tagtointeract;
    public GameObject PortalToEnd, platform;


    private Rigidbody2D rb, rb2;
    private bool grounded;
    private bool attack;
    //private Transform groundChecker;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb2 = platform.GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        float MoveX = Input.GetAxis("Horizontal");
        float MoveY = Input.GetAxis("Vertical");

        Vector3 newVelocity = MoveY * Speed * transform.right;
        newVelocity.y = rb.velocity.y;
        rb.velocity = newVelocity;

        if (Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector3.up * JumpForce, ForceMode2D.Impulse);
        }
    }

        private void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.gameObject.CompareTag("Button1"))
        {
            rb2.isKinematic = false;
        }
        if (collider.gameObject.CompareTag("Respawn"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Respawn"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        
        
        if (collision.gameObject.CompareTag("Finish"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

}