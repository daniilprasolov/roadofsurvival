using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public new GameObject camera;
    public int i;

    private Camera cameraa;
    void Start()
    {
        cameraa = camera.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            cameraa.orthographicSize = i;
        }
    }
}
